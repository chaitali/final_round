Feature: Create Posts 
Scenario: Save post with user 
	Given Posts can be saved 
	When Using a user from User API 
	Then Save a post with this user 
	
Scenario: Save post without title 
	Given Posts can be saved 
	When Saving a post without a title 
	Then API must return an error
Feature: Get users
Scenario: Users must have basic details 
	Given API is reachable 
	When Requesting all users from api 
	Then All users must have a name, username, and email 
	
Scenario: User email must be valid 
	Given API is reachable 
	When Requesting all users from api 
	Then Their email must be valid 
	
Scenario: Company catchphrase must be valid 
	Given API is reachable 
	When Requesting all users from api 
	Then Their Company catchphrase must have less than 50 characters 
	
Scenario: All validations should pass 
	Given API is reachable 
	When Requesting all users from api 
	Then All users must have a name, username, and email 
	And Their email must be valid 
	And Their Company catchphrase must have less than 50 characters
package apitests.cucumber.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import java.net.HttpURLConnection;

import apitests.endpoint.PostsEndpoint;
import apitests.endpoint.UsersEndpoint;
import apitests.model.Post;
import apitests.model.User;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PostStepDefinition {

	PostsEndpoint postsEndpoint = new PostsEndpoint();
	UsersEndpoint usersEndpoint = new UsersEndpoint();
	User testUser;
	int response;

	@Given("^Posts can be saved$")
	public void givenPostsCanBeSaved() {
		assertEquals(postsEndpoint.ping(), HttpURLConnection.HTTP_OK);
	}

	@When("^Using a user from User API$")
	public void whenUsingAUserFromUserApi() {
		testUser = usersEndpoint.getAllUsers().get(0);
		assertNotNull(testUser);
	}

	@Then("^Save a post with this user$")
	public void thenSaveAPostWithThisUser() {
		Post post = new Post(0, testUser.getId(), "Test Post 1 Title", "Test Post 1 Body");
		response = postsEndpoint.savePost(post);
		System.out.println("thenSaveAPostWithThisUser(): " + response);
		assertEquals(response, HttpURLConnection.HTTP_CREATED);
	}

	@When("^Saving a post without a title$")
	public void whenSavingAPostWithoutATitle() {
		Post post = new Post(0, 0, null, "Test Post 2 Body");
		response = postsEndpoint.savePost(post);
		System.out.println("whenSavingAPostWithoutATitle(): " + response);
	}

	@Then("^API must return an error$")
	public void thenApiMustReturnAnError() {
		assertNotEquals(response, HttpURLConnection.HTTP_CREATED);
	}

}

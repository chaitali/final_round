package apitests.cucumber.step;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.regex.Pattern;

import org.testng.asserts.SoftAssert;

import apitests.endpoint.UsersEndpoint;
import apitests.model.User;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UserStepDefinition {

	List<User> users;
	UsersEndpoint usersEndpoint = new UsersEndpoint();
	
	@Given("^API is reachable$")
	public void apiIsReachable() {
		assertEquals(usersEndpoint.ping(), HttpURLConnection.HTTP_OK);
	}

	@When("^Requesting all users from api$")
	public void requestingAllUsersFromApi() {
		users = usersEndpoint.getAllUsers();
		assertNotNull(users);
	}

	@Then("^All users must have a name, username, and email$")
	public void allUsersMustHaveNameUsernameAndEmail() {

		SoftAssert softAssert = new SoftAssert();

		users.forEach(u -> {
			softAssert.assertFalse(u.getName().isEmpty());
			softAssert.assertFalse(u.getEmail().isEmpty());
			softAssert.assertFalse(u.getUsername().isEmpty());
		});

		softAssert.assertAll();
	}

	@Then("^Their email must be valid$")
	public void theirEmailMustBeValid() {

		SoftAssert softAssert = new SoftAssert();

		users.forEach(u -> {
			softAssert.assertTrue(validateEmail(u.getEmail()));
		});

		softAssert.assertAll();
	}
	
	@Then("^Their Company catchphrase must have less than 50 characters$")
	public void theirCompanyCatchphraseMustHaveLessThan50Characters() {
		SoftAssert softAssert = new SoftAssert();

		users.forEach(u -> {
			softAssert.assertTrue(
					(u.getCompany().getCatchPhrase() != null)
					&&
					(!u.getCompany().getCatchPhrase().isEmpty())
					&&
					(u.getCompany().getCatchPhrase().length() < 50));
		});

		softAssert.assertAll();
	}
	
	public boolean validateEmail(String email) {		
		return Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)
				.matcher(email)
				.matches();

	}
}

package apitests.cucumber;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
	features="src/test/java/apitests/cucumber/feature",
	glue="apitests.cucumber.step"
		)
public class TestRunner extends AbstractTestNGCucumberTests {

	
}

package apitests.endpoint;

import java.util.List;

import apitests.model.User;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UsersEndpoint extends ApiEndpoint {

	private static final String URI = "/users";

	@Override
	String getUri() {
		return URI;
	}

	@Override
	public int ping() {
		RestAssured.baseURI = getBaseUri();
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.get(URI);
		return response.getStatusCode();
	}

	public List<User> getAllUsers() {

		RestAssured.baseURI = getBaseUri();

		return RestAssured.given().when().get(URI).then().assertThat().statusCode(200).assertThat().extract().body()
				.jsonPath().getList("", User.class);
	}

}

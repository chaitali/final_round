package apitests.endpoint;

import apitests.model.Post;
import apitests.util.MimeTypes;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PostsEndpoint extends ApiEndpoint {

	private static final String URI = "/posts";
	
	@Override
	String getUri() {
		return URI;
	}
	
	@Override
	public int ping() {
		RestAssured.baseURI = getBaseUri();
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.get(URI);
		return response.getStatusCode();
	}
	
	public int savePost(Post post) {
		
		RestAssured.baseURI = getBaseUri();
		
		Response response = RestAssured.given()
			.contentType(MimeTypes.Application.JSON)
			.body(post)
			.when().post(URI);
		
		return response.getStatusCode();
	}

}

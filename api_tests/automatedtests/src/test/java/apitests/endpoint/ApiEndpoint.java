package apitests.endpoint;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public abstract class ApiEndpoint {
	
	private static final String BASE_URI = "https://jsonplaceholder.typicode.com/";
	
	abstract String getUri();
	
	public int ping() {
		RestAssured.baseURI = getBaseUri();
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.get(BASE_URI);
		return response.getStatusCode();
	}
	
	public static final String getBaseUri() {
		return BASE_URI;
	}

}

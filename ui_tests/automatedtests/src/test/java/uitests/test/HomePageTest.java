package uitests.test;

import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import uitests.page.HomePage;

public class HomePageTest extends BaseTest {

	HomePage homePage;
	SoftAssert softAssert;

	@BeforeClass
	public void setup() {
		homePage = new HomePage(driver);
		PageFactory.initElements(driver, homePage);
		softAssert = new SoftAssert();
	}

	@Test
	public void testSearchSuggestions() {
		Reporter.log("Test Case ID #1: Verify search suggestions are relevant to the search query");
		homePage.inputSearchText("anything");
	}

	@Test
	public void verifyProductQuickView() {
		homePage.navigateToProductList();

		// Assert if all required details are mentioned on Quick View Hover
		softAssert.assertTrue(homePage.btnQuickView.isDisplayed(), "Quick View button is not displayed");
		softAssert.assertTrue(homePage.btnAddToCart.isDisplayed(), "Add to Cart button is not displayed");
		softAssert.assertTrue(homePage.btnViewMore.isDisplayed(), "View More button is not displayed");
	}

	@AfterClass
	public void afterClass() {
		softAssert.assertAll();
	}
}

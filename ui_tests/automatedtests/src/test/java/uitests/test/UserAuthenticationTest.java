package uitests.test;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import uitests.page.UserAuthenticationPage;

public class UserAuthenticationTest extends BaseTest {

	UserAuthenticationPage userAuthenticationPage;
	SoftAssert softAssert;

	@BeforeClass
	public void setup() {
		userAuthenticationPage = new UserAuthenticationPage(driver);
		PageFactory.initElements(driver, userAuthenticationPage);
		softAssert = new SoftAssert();
	}

	@Test
	public void verifyUserRegistration() {
		try {
			userAuthenticationPage.registerUser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		softAssert.assertTrue(driver.getTitle().equals("Login - My Store"));
	}

	@Test
	public void verifyUserRegistrationSuccess() {
		userAuthenticationPage.submitRegistrationCorrectForm();
		softAssert.assertTrue(driver.getTitle().contains("My account - My Store"));

	}

	@AfterClass
	public void afterClass() {
		softAssert.assertAll();
	}

}
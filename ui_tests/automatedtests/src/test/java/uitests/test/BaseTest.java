package uitests.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {

	public static final String baseUrl = "http://automationpractice.com/index.php";
	public WebDriver driver;

	@BeforeClass
	public void init() {
		System.out.println("init()");
		System.setProperty("webdriver.gecko.driver",
				"src\\test\\resources\\geckodriver.exe");
		//C:\\Users\\TheJoker273\\.m2\\repository\\org\\openqa\\selenium\\firefox\\driver\\geckodriver.exe
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(baseUrl);
	}

	@AfterClass
	public void tearDown() {
		System.out.println("tearDown()");
		 driver.close();
	}

}

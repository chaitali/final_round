package uitests.test;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import uitests.page.CartPage;

public class CartTest extends BaseTest {

	CartPage cartPage;
	SoftAssert softAssert;

	@BeforeClass
	public void setup() {
		cartPage = new CartPage(driver);
		PageFactory.initElements(driver, cartPage);
		softAssert = new SoftAssert();
	}

	@Test
	public void verifyProductIsAddedToCart() {

		cartPage.navigateToCategory();
		cartPage.selectSubCategory();
	}

	@Test(dependsOnMethods = "verifyProductIsAddedToCart")
	public void verifyCheckoutPage() {
		cartPage.navigateToProductDetailAndAddToCart();
		cartPage.checkoutWithLogin();
	}

	@Test(dependsOnMethods = "verifyCheckoutPage")
	public void verifyShippingAndCheckout() {
		cartPage.verifyAddressInCheckout();
		cartPage.agreeToShippingTerms();
//		softAssert.assertTrue(cartPage.shippingNavCurrentTab.isDisplayed());
	}

	@Test(dependsOnMethods = "verifyShippingAndCheckout")
	public void verifyPaymentMethodSelection() {
		cartPage.payByBankWire();
//		softAssert.assertEquals(cartPage.headerPaymentMethod.getText(), "Bank-wire payment.");
	}

	@Test(dependsOnMethods = "verifyPaymentMethodSelection")
	public void verifyConfirmOrder() {
		cartPage.confirmOrder();

	}

	@AfterClass
	public void afterClass() {
		softAssert.assertAll();
	}

}
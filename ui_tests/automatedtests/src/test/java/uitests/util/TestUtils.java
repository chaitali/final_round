package uitests.util;

import java.util.UUID;

public class TestUtils {

	public static final String searchInputText = "Blouse";
	public static final String[] menuItems = { "Women", "T-shirts", "Dresses" };
	public static final String invalidEmailSystemValidationMesaage = "Invalid email address.";
	public static final String userEmail = "qa@test.com";
	public static final String userPass = "12345";
	public static final String orderComment = "Please leave the package at the back-door.";

	public static String randomEmail() {
		return "random-" + UUID.randomUUID().toString() + "@example.com";
	}
}

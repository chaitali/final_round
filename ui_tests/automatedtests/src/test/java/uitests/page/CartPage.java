package uitests.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import uitests.util.PageUtils;
import uitests.util.TestUtils;

public class CartPage {

	protected static WebDriver driver;

	public CartPage(WebDriver driver) {
		super();
		this.driver = driver;
	}

	// All Page Factory FindBy annotations go here
	@FindBy(xpath = "//li[@class='last']//a[contains(text(),'Women')]")
	public WebElement productCategoryInFooter;

	@FindBy(xpath = "//div[@class='subcategory-image']//a[@title='Dresses']")
	public WebElement subCategoryTwo;

	@FindBy(xpath = "//div[@class='subcategory-image']//a[@title='Summer Dresses']")
	public WebElement subCategorychildThree;

	@FindBy(xpath = "//h5[@itemprop='name']")
	public WebElement productName;

	@FindBy(xpath = "//i[@class='icon-th-list']")
	public WebElement listView;

	@FindBy(xpath = "//span[contains(text(),'Add to cart')]")
	public WebElement addToCartButton;

	@FindBy(xpath = "//span[@id='layer_cart_product_title']")
	public WebElement cartproductName;

	@FindBy(xpath = "//span[contains(text(),'Proceed to checkout')]")
	public WebElement proceedToCheckout;

	@FindBy(xpath = "//span[@class='label label-success']")
	public WebElement productInStock;

	@FindBy(xpath = "//span[@id='product_price_5_19_0']//span[@class='price']")
	public WebElement unitPrice;

	@FindBy(xpath = "//td[@class='cart_quantity text-center']//input[@type='text']")
	public WebElement quantityPerProduct;

	@FindBy(xpath = "//span[@id='total_product_price_5_19_0']")
	public WebElement totalPrice;

	@FindBy(xpath = "//span[contains(text(), 'Proceed to checkout')]")
	public WebElement btnProceedToCheckout;

	@FindBy(xpath = "//h1[@id='cart_title']")
	public WebElement summaryTitle;

	@FindBy(xpath = "//li[@class='step_current third']")
	public WebElement addressNavCurrentTab;

	@FindBy(xpath = "//li[@class='step_current four']//span[contains(text(),'Shipping')]")
	public WebElement shippingNavCurrentTab;

	@FindBy(xpath = "//input[@id='cgv']")
	public WebElement cbTerms;

	@FindBy(xpath = "//a[@class='bankwire']")
	public WebElement btnWireTransfer;

	@FindBy(xpath = "//strong[contains(text(),'You have chosen to pay by bank wire.')]")
	public WebElement confirmPaymentMethod;

	@FindBy(xpath = "//span[contains(text(),'I confirm my order')]")
	public WebElement btnConfirmOrder;

	@FindBy(xpath = "//strong[contains(text(),'Your order on My Store is complete.')]")
	public WebElement msgOrderComplete;

	@FindBy(xpath = "//a[@class='product-name']")
	public WebElement productN;

	@FindBy(xpath = "//input[@id='email']")
	public WebElement loginEmail;

	@FindBy(xpath = "//input[@id='passwd']")
	public WebElement loginPass;

	@FindBy(xpath = "//i[@class='icon-lock left']")
	public WebElement btnSignIn;

	@FindBy(xpath = "//a[@class='button btn btn-default standard-checkout button-medium']")
	public WebElement btnCheckoutInCartSummary;

	@FindBy(xpath = "//button[@type='submit']//span[contains(text(),'Proceed to checkout')]")
	public WebElement btnCheckoutInAddress;

	@FindBy(xpath = "//textarea[@class='form-control']")
	public WebElement txtOrderComment;

	@FindBy(xpath = "//h3[@class='page-subheading']")
	public WebElement headerPaymentMethod;

	public void navigateToCategory() {

		PageUtils.scrollToElement(driver, productCategoryInFooter);
		productCategoryInFooter.click();
	}

	public void selectSubCategory() {

		PageUtils.waitForElementToLoad(driver, subCategoryTwo, 5);
		subCategoryTwo.click();
		subCategorychildThree.click();

		PageUtils.waitForElementToLoad(driver, listView, 5);
		listView.click();
		PageUtils.waitForElementToLoad(driver, productName, 5);

	}

	public void navigateToProductDetailAndAddToCart() {
		productN.click();
		addToCartButton.click();
		PageUtils.waitForElementToBeVisible(driver, btnProceedToCheckout, 5);
		btnProceedToCheckout.click();
	}

	public void checkoutWithLogin() {
		PageUtils.waitForElementToBeVisible(driver, btnCheckoutInCartSummary, 5);
		btnCheckoutInCartSummary.click();
		loginEmail.sendKeys(TestUtils.userEmail);
		loginPass.sendKeys(TestUtils.userPass);
		btnSignIn.click();
	}

	public void verifyAddressInCheckout() {
		txtOrderComment.sendKeys(TestUtils.orderComment);
		PageUtils.waitForElementToBeVisible(driver, btnCheckoutInAddress, 5);
		btnCheckoutInAddress.click();
	}

	public void agreeToShippingTerms() {
		cbTerms.click();
		PageUtils.waitForElementToBeVisible(driver, btnCheckoutInAddress, 5);
		btnCheckoutInAddress.click();
	}

	public void payByBankWire() {
		btnWireTransfer.click();
		PageUtils.waitForElementToBeVisible(driver, btnConfirmOrder, 5);
		btnConfirmOrder.click();
	}

	public void confirmOrder() {
		PageUtils.waitForElementToBeVisible(driver, btnConfirmOrder, 5);
		btnConfirmOrder.click();
	}

	public void shipping() {
		cbTerms.click();
		btnProceedToCheckout.click();
	}

	public void payment() {
		btnWireTransfer.click();
		btnConfirmOrder.click();

	}

}
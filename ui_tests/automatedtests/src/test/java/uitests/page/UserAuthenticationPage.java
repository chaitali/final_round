package uitests.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.log4testng.Logger;

import uitests.util.PageUtils;
import uitests.util.TestUtils;

public class UserAuthenticationPage {

	protected static WebDriver driver;
	Logger logger = Logger.getLogger(UserAuthenticationPage.class);

	final String randomEmail = TestUtils.randomEmail();
	final String userEmail = "qa@test.com";
	final String userPass = "12345";
	public String emptyEmailErrorMessage;

	@FindBy(xpath = "//a[@class='login']")
	public WebElement btnSignIn;

	@FindBy(xpath = "//input[@id='email_create']")
	public WebElement inputRegistrationEmail;

	@FindBy(xpath = "//input[@id='email_create' and @class='is_required validate account_input form-control']")
	public WebElement emptyEmailInputFieldBoxColorError;

	@FindBy(xpath = "//div[@id='create_account_error']//ol//li")
	public WebElement emptyEmailAlertMessage;

	@FindBy(xpath = "//button[@id='SubmitCreate']")
	public WebElement signUpSubmitButton;

	@FindBy(xpath = "//button[@id='submitAccount']")
	public WebElement registerSubmitButton;

	@FindBy(xpath = "//input[@id='customer_firstname']")
	public WebElement inputFirstName;

	@FindBy(xpath = "//input[@id='customer_lastname']")
	public WebElement inputLastName;

	@FindBy(xpath = "//input[@id='passwd']")
	public WebElement inputPassword;

	@FindBy(xpath = "//input[@id='firstname']")
	public WebElement inputFirstNameForAddress;

	@FindBy(xpath = "//input[@id='lastname']")
	public WebElement inputLastNameForAddress;

	@FindBy(xpath = "//input[@id='address1']")
	public WebElement inputAddress;

	@FindBy(xpath = "//input[@id='city']")
	public WebElement inputCity;

	@FindBy(xpath = "//option[@value='38']")
	public WebElement selectState;

	@FindBy(xpath = "//input[@id='postcode']")
	public WebElement inputZipOrPostalCode;

	@FindBy(xpath = "//select[@id='id_country']")
	public WebElement selectCountry;

	@FindBy(xpath = "//input[@id='phone_mobile']")
	public WebElement inputMobilePhone;

	@FindBy(xpath = "//input[@id='alias']")
	public WebElement inputAddressAlias;

	@FindBy(xpath = "//span[contains(text(),'Home')]")
	public WebElement navigateBackToHomeBtn;

	@FindBy(xpath = "//a[@class='logout']")
	public WebElement signOutBtn;


	public UserAuthenticationPage(WebDriver driver) {
		this.driver = driver;
	}

	public void registerUser() {
		btnSignIn.click();
		inputRegistrationEmail.sendKeys(randomEmail);
		signUpSubmitButton.click();
	}

	public void submitRegistrationCorrectForm() {

		PageUtils.waitForElementToLoad(driver, inputFirstName, 5);
		inputFirstName.sendKeys("John");
		inputLastName.sendKeys("Doe");
		inputPassword.sendKeys(userPass);
		inputFirstNameForAddress.sendKeys("John");
		inputLastNameForAddress.sendKeys("Doe");
		inputAddress.sendKeys("Addres Line 1");
		inputCity.sendKeys("City");
		selectState.click();
		inputZipOrPostalCode.sendKeys("12345");
		inputMobilePhone.sendKeys("1234567890");
		PageUtils.waitForElementToBeClickable(driver, inputFirstName, 5);
		registerSubmitButton.click();
		navigateBackToHomeBtn.click();
	}

}

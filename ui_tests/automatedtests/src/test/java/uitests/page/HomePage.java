package uitests.page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import uitests.util.PageUtils;

public class HomePage {

	protected static WebDriver driver;

	@FindBy(xpath = "//input[@id='search_query_top']")
	public WebElement inputFieldSearch;

	@FindBy(xpath = "//a[@class='login']")
	public WebElement btnSignIn;

	@FindBy(xpath = "//div[@class='ac_results']/ul/li")
	public List<WebElement> searchSuggestions;

	@FindBy(xpath = "//li[@class='homeslider-container'] //img")
	public WebElement slideShow;

	@FindBy(xpath = "//div[@id='block_top_menu']/ul[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']/li")
	public List<WebElement> topMenuItems;

	@FindBy(xpath = "//h1//span[@class='cat-name']")
	public WebElement pageMenuHeading;

	@FindBy(xpath = "//div[@class='product-image-container']")
	public WebElement productContainer;

	@FindBy(xpath = "//a[@class='quick-view']")
	public WebElement btnQuickView;

	@FindBy(xpath = "//div[@class=\"content_price']")
	public WebElement labelPrice;

	@FindBy(xpath = "//a[@class='button ajax_add_to_cart_button btn btn-default']//span")
	public WebElement btnAddToCart;

	@FindBy(xpath = "//a[@class='button lnk_view btn btn-default']//span")
	public WebElement btnViewMore;

	@FindBy(xpath = "//iframe[starts-with(@id,'fancybox-frame')]")
	public WebElement iFrame;

	@FindBy(xpath = "//h1[@itemprop='name']")
	public WebElement productName;

	@FindBy(xpath = "//span[@itemprop='sku']")
	public WebElement productSKU;

	@FindBy(xpath = "//link[@itemprop='itemCondition'] /following-sibling::span")
	public WebElement productCondition;

	@FindBy(xpath = "//div[@id='short_description_content']//p")
	public WebElement productDescription;

	@FindBy(xpath = "//span[@id='our_price_display']")
	public WebElement productPrice;

	@FindBy(xpath = "//input[@id='quantity_wanted']")
	public WebElement productQuantity;

	@FindBy(xpath = "//select[@id='group_1'] //option[@selected]")
	public WebElement productSize;

	@FindBy(xpath = "//button[@type='submit']")
	public WebElement addToCart;

	@FindBy(xpath = "//a[@id='wishlist_button']")
	public WebElement addToWishlist;

	@FindBy(xpath = "//a[@class='fancybox-item fancybox-close']")
	public WebElement closeIcon;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputSearchText(String searchText) {
		inputFieldSearch.sendKeys(searchText);
	}

	public List<WebElement> getAutoCompleteSearchResults(String searchString) {
		PageUtils.waitForElementToBeClickable(driver, inputFieldSearch, 10);
		inputFieldSearch.sendKeys(searchString);

		return searchSuggestions;
	}

	public void navigateToProductList() {

		PageUtils.scrollToElement(driver, productContainer);
		PageUtils.waitForElementToLoad(driver, productContainer, 10);
		Actions actions = new Actions(driver);
		actions.moveToElement(productContainer).perform();

	}

	public void navigateToQuickViewWindow() {
		// Navigate to QuickView Window
		btnQuickView.click();
		PageUtils.waitForElementToLoad(driver, iFrame, 5);
		driver.switchTo().frame(iFrame);
	}
}
